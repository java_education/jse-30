package ru.t1.oskinea.tm.command.data;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.Domain;
import ru.t1.oskinea.tm.enumerated.Role;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from base64 file.";

    @NotNull
    public static final String NAME = "data-load-base64";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BASE64]");
        @NotNull final byte[] base64Bytes = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Bytes);
        @Cleanup @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
